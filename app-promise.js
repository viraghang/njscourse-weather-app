const yargs = require('yargs')
const ip2location = require('ip-to-location')
const publicIp = require('public-ip')

const weather = require('./weather/weather-promise')

const argv = yargs
  .options({
    a: {
      demand: false,
      alias: 'address',
      describe: 'Address to fetch weather for (defaults to current location by external IP)',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv

if (argv.address) {
  let encodedAddress = encodeURIComponent(argv.address)
  weather.getWeather(encodedAddress)
} else {
  publicIp.v4().then(ip => {
    if (ip) return ip2location.fetch(ip)
  }).then(res => {
    let encodedAddress = encodeURIComponent(`${res.city} ${res.region_code} ${res.country_code}`)
    weather.getWeather(encodedAddress)
  }).catch(err => {
    console.log(err)
  })
}
