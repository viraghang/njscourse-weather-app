const request = require('request')

const geocodeAddress = (userAddress, callback) => {
  let encodedAddress = encodeURIComponent(userAddress)
  let apiKey = 'AIzaSyCNAupDD3IWyfn70fZV4O9KFGW5FFaWRIw'
  request({
    url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=${apiKey}`,
    json: true
  }, (error, response, body) => {
    if (error) {
      callback('Unable to connect to Google servers')
    } else if (body.status === 'ZERO_RESULTS') {
      callback('Unable to find the provided address')
    } else if (body.status === 'OK') {
      callback(undefined, {
        address: body.results[0].formatted_address,
        latitude: body.results[0].geometry.location.lat,
        longitude: body.results[0].geometry.location.lng
      })
    } else {
      callback('Something went wrong')
    }
  })
}

module.exports = {
  geocodeAddress
}
