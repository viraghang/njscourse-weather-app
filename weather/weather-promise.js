const axios = require('axios')

let getWeather = encodedAddress => {
  let googleApiKey = 'AIzaSyCNAupDD3IWyfn70fZV4O9KFGW5FFaWRIw'
  let geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=${googleApiKey}`

  axios.get(geocodeUrl).then((response) => {
    if (response.data.status === 'ZERO_RESULTS') {
      throw new Error('Unable to find that address')
    }
    let darkSkyApiKey = '93900df00e00bf40653f80ad810e609b'
    let lat = response.data.results[0].geometry.location.lat
    let lng = response.data.results[0].geometry.location.lng
    let weatherUrl = `https://api.darksky.net/forecast/${darkSkyApiKey}/${lat},${lng}`
    console.log(`\nResolved location based on your input or IP:\n${response.data.results[0].formatted_address}\n`)
    return axios.get(weatherUrl)
  }).then((response) => {
    let temperature = response.data.currently.temperature
    let apparentTemperature = response.data.currently.apparentTemperature
    let humidity = response.data.currently.humidity
    let uvIndex = response.data.currently.uvIndex
    console.log(`It's currently ${temperature}.\nIt feels like ${apparentTemperature}.\nHumidity is ${humidity}.\nThe UV Index is ${uvIndex}.\n`)
  }).catch((e) => {
    if (e.code === 'ENOTFOUND') {
      console.log('Unable to connect to API services')
    } else {
      console.log(e.message)
    }
  })
}

module.exports = {
  getWeather
}
