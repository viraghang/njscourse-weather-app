const request = require('request')

let geocodeAddress = (address) => {
  let encodedAddress = encodeURIComponent(address)
  let apiKey = 'AIzaSyCNAupDD3IWyfn70fZV4O9KFGW5FFaWRIw'
  return new Promise((resolve, reject) => {
    request({
      url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=${apiKey}`,
      json: true
    }, (error, response, body) => {
      if (error) {
        reject('Unable to connect to Google servers')
      } else if (body.status === 'ZERO_RESULTS') {
        reject('Unable to find the provided address')
      } else if (body.status === 'OK') {
        resolve({
          address: body.results[0].formatted_address,
          latitude: body.results[0].geometry.location.lat,
          longitude: body.results[0].geometry.location.lng
        })
      } else {
        reject('Something went wrong')
      }
    })
  })
}

geocodeAddress('0000').then((location) => {
  console.log(JSON.stringify(location, null, 2))
}, (errorMessage) => {
  console.log(errorMessage)
})
