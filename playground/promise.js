let asyncAdd = (a, b) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (typeof a === 'number' && typeof b === 'number') {
        resolve(a + b)
      } else {
        reject('Arguments must be numbers')
      }
    }, 1000)
  })
}

asyncAdd(3, '4').then((res) => {
  console.log(res)
  return asyncAdd(res, 33)
}).then((res) => {
  console.log('Should be 40', res)
}).catch((errorMessage) => {
  console.log(errorMessage)
})

// let somePromise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     resolve('Hey. It worked!')
//     reject('Hey. It is fucked up!')
//   }, 2500)
// })
//
// somePromise.then((message) => {
//   console.log(message)
// }, (errorMessage) => {
//   console.log(errorMessage)
// })
